// The vertex shader is just the passthrough vertex shader that Game Maker:
// Studio generates when you make a new shader. None of us wrote this part, and
// we won't pretend we did. We did write the fragment shader though.

attribute vec3 in_Position;                  // (x,y,z)
attribute vec4 in_Colour;                    // (r,g,b,a)
attribute vec2 in_TextureCoord;              // (u,v)

varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
    vec4 object_space_pos = vec4( in_Position.x, in_Position.y, in_Position.z, 1.0);
    gl_Position = gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * object_space_pos;
    
    v_vColour = in_Colour;
    v_vTexcoord = in_TextureCoord;
}
//######################_==_YOYO_SHADER_MARKER_==_######################@~// Copyright (C) 2018 Team Circadia.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// If you want to understand what's going on here, read this first:
// https://gitlab.com/team-circadia/crystal-shader/wikis/release-features-and-reference#how-it-works

#ifdef GL_ES
precision mediump float;
#endif

varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform sampler2D map_texture;
uniform sampler2D palette_texture;
uniform vec2 texel_size;
uniform vec4 palette_UVs;
uniform float palette_index;

void main()
{
    vec4 source = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
    
    vec4 col_id = texture2D(map_texture, vec2(
        // This same maths (except legible) is also in _palette_hash.
        ((floor(source.r*255.0/8.0)*8.0)+floor(source.g*255.0/32.0))/256.0+1.0/512.0,
        ((floor(source.b*255.0/8.0)*8.0)+floor(fract(source.g*255.0/32.0)*8.0))/256.0+1.0/512.0
        ));
    // Parts of these calculations could be put on the CPU instead,
    // but it would make the code significantly harder to read.
    // Also it only saves 3 instructions, and we're well below the limit.
    float yy = palette_UVs.y + texel_size.y * (col_id.r * 255.0);
    float xx = palette_UVs.x + texel_size.x * palette_index;
    
    vec4 other = texture2D(palette_texture, vec2(xx, yy));
    // It seems a bit unintuitive, but on older computers, you can't put
    // texture reads inside if blocks, so it has to be done this way.
    if (col_id.a != 0.0) {
        other.a *= source.a; // include the alpha from the source pixel
        source = other;
    }
    gl_FragColor = source;
}
