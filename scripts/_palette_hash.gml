///_palette_hash(red, green, blue)
// Calculates where to place the colour on the palette sheet, and
// returns the coordinates as a 2-value array.
// These same calculations are also in the actual shader code.

// Copyright (C) 2018 Team Circadia.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

var coords;
coords[0] = (argument[0] & $f8) + (argument[1] >> 5);
coords[1] = (argument[2] & $f8) + ((argument[1] & $1c) >> 2);
return coords;
