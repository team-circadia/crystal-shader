///palette_reset(palette)
// Call anywhere to reset the colors on a palette.

// Copyright (C) 2018 Team Circadia.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

var spr = argument0;

if (_palette_check(spr)) {
    // If _palette_check calls this script, then any subsequent call to
    // _palette_check will return false, so the script runs.
    // If _palette_check calls this script from within another palette_reset call,
    // _palette_check will return false on the inner call and true on the outer,
    // so the script only gets run once.
    exit;
}

var pal_set = global.palette_sets[? spr];
var pal_s = pal_set[| 0];
var spr_b = pal_set[| 1];
var recolor_queue = pal_set[| 2];

if (event_type != ev_draw) {
    ds_queue_enqueue(recolor_queue, -1, -1);
    exit;
}

shader_reset();

surface_set_target(pal_s);
draw_clear_alpha(0, 0);

buffer_seek(spr_b, buffer_seek_start, 0);
for (var i = 0; i < sprite_get_height(spr); i++) {
    var col;
    for (var j = 0; j < 4; j++)
        col[j] = buffer_read(spr_b, buffer_u8);
    blue_id = global.palette_format[0];
    green_id = global.palette_format[1];
    red_id = global.palette_format[2];
    alpha_id = global.palette_format[3];
    b = col[blue_id];
    g = col[green_id];
    r = col[red_id];
    a = col[alpha_id];
    if (a == 0) continue;
    var coords = _palette_hash(r, g, b);
    draw_point_color(coords[0], coords[1], i);
}
surface_reset_target();
