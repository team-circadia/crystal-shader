///palette_init()
// Initiates the palette system.  Call once at the beginning of your game somewhere.
// All global variables that the system uses are initialized in this script.

// Copyright (C) 2018 Team Circadia.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// Uniforms that allow us to communicate with the shader.
global.uni_texel_size = shader_get_uniform(shdPalSwapper, 'texel_size');
global.uni_palette_uvs = shader_get_uniform(shdPalSwapper, 'palette_UVs');
global.uni_palette_index = shader_get_uniform(shdPalSwapper, 'palette_index');
global.uni_palette_texture = shader_get_sampler_index(shdPalSwapper, 'palette_texture');
global.uni_map_texture = shader_get_sampler_index(shdPalSwapper, 'map_texture');

// A ds_map where the keys are sprite indices for palette sheets, and the
// values are ds_lists with three items: the palette map, a buffer containing
// the leftmost column of the palette sheet, and a queue which is used to allow
// palette_recolor and palette_reset to be called outside of the draw event,
// as they both involve drawing, which is not guaranteed to work outside of the
// draw event.
global.palette_sets = ds_map_create();

// An array to specify the pixel format of the platform. This is important when
// reading surfaces from buffers, and necessary because not every platform uses
// the same format. Each pixel consists of four bytes. The entries in this
// array indicate which of the four bytes refers to the blue, green, and alpha
// components of the pixel's color, in that order, as an integer from 0 (the
// first byte) to 3 (the fourth byte). The array is initialized in the first
// call to _palette_add.
global.palette_format = noone;
