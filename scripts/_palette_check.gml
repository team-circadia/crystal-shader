///_palette_check(palette)
// Checks if the palette is ready to use. If it is, return false.
// Otherwise, fix it and return true.

// Copyright (C) 2018 Team Circadia.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

var spr = argument0;

var pal_set = global.palette_sets[? spr];
if (is_undefined(pal_set)) {
    // _palette_add has not yet been called
    _palette_add(spr);
    return true;
}
if (!buffer_exists(pal_set[| 1])) {
    // _palette_add has been called outside the draw event
    if (event_type == ev_draw)
        _palette_add(spr);
    return true;
}
if (!surface_exists(pal_set[| 0]) && event_type == ev_draw) {
    // something happened that caused the surfaces to be deleted
    pal_set[| 0] = surface_create(256, 256);
    palette_reset(spr);
    return true;
}

return false;
