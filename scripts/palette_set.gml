///palette_set(palette, palette_index)
// Sets the shader to recolor using the given palette and index.
// Call in the draw event before drawing whatever you want recolored.
// After you're done drawing, call shader_reset.

// Copyright (C) 2018 Team Circadia.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

var spr = argument[0];
var pal_index = argument[1];

_palette_check(spr);
var pal_set = global.palette_sets[? spr];

var recolor_queue = pal_set[| 2];
while (!ds_queue_empty(recolor_queue)) {
    var old_col = ds_queue_dequeue(recolor_queue);
    var new_col = ds_queue_dequeue(recolor_queue);
    if (old_col >= 0)
        palette_recolor(spr, old_col, new_col);
    else
        palette_reset(spr);
}

shader_set(shdPalSwapper);
var map_tex = surface_get_texture(pal_set[| 0]);
var pal_tex = sprite_get_texture(spr, 0);
var UVs = sprite_get_uvs(spr, 0);

texture_set_stage(global.uni_palette_texture, pal_tex);
texture_set_stage(global.uni_map_texture, map_tex);

var texel_x = texture_get_texel_width(pal_tex);
var texel_y = texture_get_texel_height(pal_tex);
var texel_hx = texel_x * 0.5;
var texel_hy = texel_y * 0.5;

shader_set_uniform_f(global.uni_texel_size, texel_x, texel_y);
shader_set_uniform_f(global.uni_palette_uvs, UVs[0] + texel_hx, UVs[1] + texel_hy, UVs[2] + texel_hx, UVs[3] + texel_hy);
shader_set_uniform_f(global.uni_palette_index, pal_index);
