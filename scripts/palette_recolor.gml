///palette_recolor(palette, old_id0, new_id0, old_id1, new_id1, ...)
// Call anywhere to replace colors on the given palette.

// Copyright (C) 2018 Team Circadia.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

var spr = argument[0];

_palette_check(spr);
var pal_set = global.palette_sets[? spr];
var pal_s = pal_set[| 0];
var spr_b = pal_set[| 1];
var recolor_queue = pal_set[| 2];

if (event_type != ev_draw) {
    for (var i = 1; i < argument_count - 1; i += 2) {
        ds_queue_enqueue(recolor_queue, argument[i], argument[i+1]);
    }
    exit;
}

shader_reset();

surface_set_target(pal_s);

for (var i = 1; i < argument_count - 1; i += 2) {
    var old_id = argument[i];
    var new_id = argument[i+1];
    buffer_seek(spr_b, buffer_seek_start, old_id * 4);
    var col;
    for (var j = 0; j < 4; j++)
        col[j] = buffer_read(spr_b, buffer_u8);
    blue_id = global.palette_format[0];
    green_id = global.palette_format[1];
    red_id = global.palette_format[2];
    alpha_id = global.palette_format[3];
    b = col[blue_id];
    g = col[green_id];
    r = col[red_id];
    a = col[alpha_id];
    if (a == 0) continue;
    var coords = _palette_hash(r, g, b);
    draw_point_color(coords[0], coords[1], new_id);
}

surface_reset_target();
