///palette_quit()
// Takes down the palette system.  Call once at the end of your game somewhere.

// Copyright (C) 2018 Team Circadia.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

var palette_sets = global.palette_sets;

for (var key = ds_map_find_first(palette_sets); !is_undefined(key); key = ds_map_find_next(palette_sets, key)) {
    var pal_set = palette_sets[? key];
    if (!is_undefined(pal_set[| 0]) && surface_exists(pal_set[| 0]))
        surface_free(pal_set[| 0]);
    if (!is_undefined(pal_set[| 1]) && buffer_exists(pal_set[| 1]))
        buffer_delete(pal_set[| 1]);
    if (!is_undefined(pal_set[| 2]) && ds_exists(pal_set[| 2], ds_type_queue))
        ds_queue_destroy(pal_set[| 2]);
}
ds_map_destroy(palette_sets);
