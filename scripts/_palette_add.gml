///_palette_add(palette)
// Add a new palette. This should be called automatically.

// Copyright (C) 2018 Team Circadia.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

var spr = argument0;

if (event_type == ev_draw) {
    // surfaces like to have power-of-2 dimensions so round to the next power of 2
    var buf_size = power(2, ceil(log2(sprite_get_height(spr))));
    var spr_s = surface_create(1, buf_size);
    var spr_b = buffer_create(1*buf_size*4, buffer_fast, 1);
    
    shader_reset();
    
    // get the colour format if we haven't already done so
    if (global.palette_format == noone) {
        surface_set_target(spr_s);
        draw_clear_alpha($123456, 1);
        surface_reset_target();
        buffer_get_surface(spr_b, spr_s, 0, 0, 0);
        buffer_seek(spr_b, buffer_seek_start, 0);
        for (var i = 0; i < 4; i++) {
            var c = buffer_read(spr_b, buffer_u8);
            if (c == $12) // blue
                global.palette_format[0] = i;
            else if (c == $34) // green
                global.palette_format[1] = i;
            else if (c == $56) // red
                global.palette_format[2] = i;
            else if (c == $ff) // alpha
                global.palette_format[3] = i;
        }
        buffer_seek(spr_b, buffer_seek_start, 0);
    }
    
    surface_set_target(spr_s);
    draw_clear_alpha(0, 0);
    draw_sprite(spr, 0, 0, 0);
    surface_reset_target();
    buffer_get_surface(spr_b, spr_s, 0, 0, 0);
    surface_free(spr_s);
    
    var pal_s = surface_create(256, 256);
} else {
    pal_s = undefined;
    spr_b = undefined;
}

var pal_set = global.palette_sets[? spr];
if (is_undefined(pal_set)) {
    pal_set = ds_list_create();
    ds_map_add_list(global.palette_sets, spr, pal_set);
}
pal_set[| 0] = pal_s;
pal_set[| 1] = spr_b;
if (is_undefined(ds_list_find_value(pal_set, 2)))
    pal_set[| 2] = ds_queue_create();

palette_reset(spr);
